@extends('base')

@section('title') Edit Profile @stop

@section('css')
  <link href="{{ asset('css/default/style.css') }}" rel="stylesheet" type="text/css" />
@stop

@section('content')
  @include('_partials.header')
  @include('_partials.nav')

  <div class="profile_cont">
    <div class="container">
      <div class="col-left profile_view">
        <div class="profile">
          <h1 class="tit_b"> Status : Contact me and ask me about my staus</h1>
          <div class="row">
            <div class="col-sm-4 text-center">
              <a href="#" class="thumbnail">
                <img src="{{ asset('imgs/default/los.jpg') }}">
              </a>
              <a href="#" class="btn btn-danger"> Edit profile</a>
            </div>
            <div class="col-sm-4">
              <h1> lovinsite <a href="#"> <span class="fa fa-video-camera "></span> online Now </a></h1>
              <p class="">I can get your attention and make you happy you spent your time with me.</p>
              <p> <b> Man Looking for Woman for</b> 1-on-1 Dating, Online Friends, Swingers, Alternative Activities, Fetishes, Other Activity</p>
              <h1>Male 49</h1>
              <table>
                <tr>
                  <th> Gender </th>
                  <td>Male</td>
                </tr>
                <tr>
                  <th> Age </th>
                  <td> 24 Years </td>
                </tr>
                <tr>
                  <th> Location </th>
                  <td>New York , United States</td>
                </tr>
                <tr>
                  <th> Sign: </th>
                  <td> Pisces </td>
                </tr>
                <tr>
                  <th> Height: </th>
                  <td> 5' 10" </td>
                </tr>
                <tr>
                  <th> Body Type: </th>
                  <td> Petite </td>
                </tr>
                <tr>
                  <th> Sexual Orientation: </th>
                  <td> Straight </td>
                </tr>
                <tr>
                  <th> Foot size: </th>
                  <td> 38</td>
                </tr>
                <tr>
                  <th> Marital Stats: </th>
                  <td> Tell ya later</td>
                </tr>
              </table>
            </div>
            <div class="col-sm-4">
              <ul class="edit">
                <li><a class="btn_text" href="#" data-toggle="modal" data-target="#editStatus"><i class="fa fa-edit"></i>Edit My Status</a></li>
                <li><a class="btn_text" href="#" data-toggle="modal" data-target="#editScreenname"><i class="fa fa-edit"></i>Edit My screenname</a></li>
                <li><a class="btn_text" href="#" data-toggle="modal" data-target="#editHeadline"><i class="fa fa-edit"></i>Edit My headline</a></li>
                <li><a class="btn_text" href="#" data-toggle="modal" data-target="#editLookingFor"><i class="fa fa-edit"></i> Edit what I'm Looking For</a></li>
                <li><a class="btn_text" href="#" data-toggle="modal" data-target="#editLocation"><i class="fa fa-edit"></i> Edit My location</a></li>
                <li><a class="btn_text" href="#" data-toggle="modal" data-target="#editProfile"><i class="fa fa-edit"></i>Edit My profile info</a></li>
              </ul>
              <!-- Modals -->
              @include('_partials.modals.profile.edit-status')
              @include('_partials.modals.profile.edit-screenname')
              @include('_partials.modals.profile.edit-headline')
              @include('_partials.modals.profile.edit-looking-for')
              @include('_partials.modals.profile.edit-location')
              @include('_partials.modals.profile.edit-profile')
              <!--/ Modals  -->
            </div>
          </div>
        </div>
        <div class="profile">
          <div class="row">
            <div class="col-sm-5">
              <h1 class="tit_b">Additional Photos<a class="btn_text inner_b" href="#"><i class="fa fa-edit"></i>Edit Info</a></h1>
              <div class="matchb">
                <ul>
                  <li>
                    <a href="#"><img src="{{ asset('imgs/default/1.jpg') }}" class="thumbnail"></a>
                    <p><a href="#">  Redlaoeluvr</a></p>
                  </li>
                  <li>
                    <a href="#"><img src="{{ asset('imgs/default/1.jpg') }}" class="thumbnail"></a>
                    <p><a href="#">  Redlaoeluvr</a></p>
                  </li>
                  <li>
                    <a href="#"><img src="{{ asset('imgs/default/1.jpg') }}" class="thumbnail"></a>
                    <p><a href="#">  Redlaoeluvr</a></p>
                  </li>
                  <li>
                    <a href="#"><img src="{{ asset('imgs/default/1.jpg') }}" class="thumbnail"></a>
                    <p><a href="#">  Redlaoeluvr</a></p>
                  </li>
                  <li>
                    <a href="#"><img src="{{ asset('imgs/default/1.jpg') }}" class="thumbnail"></a>
                    <p><a href="#">  Redlaoeluvr</a></p>
                  </li>
                  <li>
                    <a href="#"><img src="{{ asset('imgs/default/1.jpg') }}" class="thumbnail"></a>
                    <p><a href="#">  Redlaoeluvr</a></p>
                  </li>
                  <li>
                    <a href="#"><img src="{{ asset('imgs/default/1.jpg') }}" class="thumbnail"></a>
                    <p><a href="#">  Redlaoeluvr</a></p>
                  </li>
                  <li>
                    <a href="#"><img src="{{ asset('imgs/default/1.jpg') }}" class="thumbnail"></a>
                    <p><a href="#">  Redlaoeluvr</a></p>
                  </li>
                  <li>
                    <a href="#"><img src="{{ asset('imgs/default/1.jpg') }}" class="thumbnail"></a>
                    <p><a href="#">  Redlaoeluvr</a></p>
                  </li>
                </ul>
              </div>
            </div>
            <div class="col-sm-7">
              <div class="full">
                <h1 class="tit_b">About Me<a class="btn_text inner_b" href="#"><i class="fa fa-edit"></i>Edit Info</a></h1>
                <p>I am an active outgoing person.I'm an educated well placed man. Widely travelled and exposed to different life styles amd value systems. Watch sports and surf the net during spare time. Like long distance driving and short vacations to quite and serene places.
                </p>
              </div>
              <div class="full">
                <h1 class="tit_b">About my match<a class="btn_text inner_b" href="#"><i class="fa fa-edit"></i>Edit Info</a></h1>
                <p>Well, its just a wish statement. I will like her to be a pleasant person with whom I could talk on various subjects and shre each other's load and good things in life.</p>
              </div>
              <div class="full">
                <h1 class="tit_b">My Foot Fetish Fantasy Perfect Date<a class="btn_text inner_b" href="#"><i class="fa fa-edit"></i>Edit Info</a></h1>
                <p>Well, its just a wish statement. I will like her to be a pleasant person with whom I could talk on various subjects and shre each other's load and good things in life.</p>
              </div>
              <div class="full">
                <h1 class="tit_b">Interesting Qualities<a class="btn_text inner_b" href="#"><i class="fa fa-edit"></i>Edit Info</a></h1>
                <table>
                  <tr>
                    <th> Tattos </th>
                    <td>Tell ya later </td>
                  </tr>
                  <tr>
                    <th> Piercings </th>
                    <td>Tell ya later </td>
                  </tr>
                  <tr>
                    <th> Education </th>
                    <td> Tell ya later </td>
                  </tr>
                  <tr>
                    <th>Eye color: </th>
                    <td>Tell ya later </td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-right">
        <div id="home_news_box">
          <!-- Headers Starts Here -->
          <h3> Latest News </h3>
          <!-- Headers Ends Here -->
          <!-- News Starts Here -->
          <!-- Box1 Starts Here -->
          <div class="news_boxes">
            <!-- Image Starts Here -->
            <div class="home_news_img">
              <a href="#"><img alt="" src="{{ asset('imgs/default/1.jpg') }}"></a>
            </div>
            <!-- Image Ends Here -->
            <!-- Text Starts Here -->
            <div>
              <p class="news_head"><a href="#" class="news_head">Creative Brochure Design</a></p>
              <p>Creative Brochure Design Brochure Dimension: 301mm x 214mm Graphics Files Included: CorelDraw(.cdr)...</p>
            </div>
            <!-- Text Ends Here -->
          </div>
          <!-- Box1 Ends Here -->
          <!-- Box2 Starts Here -->
          <div class="news_boxes">
            <!-- Image Starts Here -->
            <div class="home_news_img">
              <a href="#"><img alt="" src="{{ asset('imgs/default/2.jpg') }}"></a>
            </div>
            <!-- Image Ends Here -->
            <!-- Text Starts Here -->
            <div>
              <p class="news_head"><a href="#" class="news_head">Modern Portfolio Theme</a></p>
              <p>Modern Portfolio Theme This PSD template is for Personal Portfolio Features: 1. Well organized layers and PSD...</p>
            </div>
            <!-- Text Ends Here -->
          </div>
          <!-- Box2 Ends Here -->
          <!-- Box3 Starts Here -->
          <div class="news_boxes">
            <!-- Image Starts Here -->
            <div class="home_news_img">
              <a href="#"><img alt="" src="{{ asset('imgs/default/3.jpg') }}"></a>
            </div>
            <!-- Image Ends Here -->
            <!-- Text Starts Here -->
            <div>
              <p class="news_head"><a href="#" class="news_head">Modern Email Template</a></p>
              <p> Modern Email Template This Email Template is for Company, Business, Products, Blogs and Portfolio...</p>
            </div>
            <!-- Text Ends Here -->
          </div>
          <!-- Box3 Ends Here -->
          <!-- Box4 Starts Here -->
          <div class="news_boxes">
            <!-- Image Starts Here -->
            <div class="home_news_img">
              <a href="#"><img alt="" src="{{ asset('imgs/default/4.jpg') }}"></a>
            </div>
            <!-- Image Ends Here -->
            <!-- Text Starts Here -->
            <div>
              <p class="news_head"><a href="#" class="news_head">Modern Under Construction Template</a></p>
              <p>Modern Under Construction Template Features Clean single page "Under Construction"...</p>
            </div>
            <!-- Text Ends Here -->
          </div>
          <!-- Box4 Ends Here -->
          <!-- Box5 Starts Here -->
          <div class="news_boxes">
            <!-- Image Starts Here -->
            <div class="home_news_img">
              <a href="#"><img alt="" src="{{ asset('imgs/default/5.jpg') }}"></a>
            </div>
            <!-- Image Ends Here -->
            <!-- Text Starts Here -->
            <div>
              <p class="news_head"><a href="#" class="news_head">Web2.0 Download Buttons</a></p>
              <p>Web2.0 Download Buttons Features 1. Organized Layer sets 2. Easy to modify 3. Editable Text 4. Change the color easily...</p>
            </div>
            <!-- Text Ends Here -->
          </div>
          <!-- Box5 Ends Here -->
          <!-- News Ends Here -->
        </div>
      </div>
    </div>
  </div>
  <div class="gallery">
    <div class="container">
      <h2>most<a href="#"> online</a> members</h2>
      <ul>
        <li>
          <div class="images_pic"> <img src="{{ asset('imgs/default/johannesM.png') }}"> </div>
          <div class="age_value">
            <div class="condidate_name">
              <p>JohannesM, 26 male
              </p>
              <p class="address"> Barcelona, Spain</p>
              <h6><a href="#">VIEW PROFILE</a></h6>
            </div>
          </div>
        </li>
        <li>
          <div class="images_pic"> <img src="{{ asset('imgs/default/HotStuff418.png') }}"> </div>
          <div class="age_value">
            <div class="condidate_name">
              <p>HotStuff418, 22 Female</p>
              <p class="address">Trujillo, Puerto Rico</p>
              <h6><a href="#">VIEW PROFILE</a></h6>
            </div>
          </div>
        </li>
        <li>
          <div class="images_pic"> <img src="{{ asset('imgs/default/SonjaG.png') }}"> </div>
          <div class="age_value">
            <div class="condidate_name">
              <p>SonjaG, 38 Female
              </p>
              <p class="address">West Bengal, India</p>
              <h6><a href="#">VIEW PROFILE</a></h6>
            </div>
          </div>
        </li>
        <li>
          <div class="images_pic"> <img src="{{ asset('imgs/default/Cuti3g4l_99.png') }}"> </div>
          <div class="age_value">
            <div class="condidate_name">
              <p>Cuti3g4l_99, 18 Female
              </p>
              <p class="address">Williams, North Dakota</p>
              <h6><a href="#">VIEW PROFILE</a></h6>
            </div>
          </div>
        </li>
        <li>
          <div class="images_pic"> <img src="{{ asset('imgs/default/Anita_C.png') }}"> </div>
          <div class="age_value">
            <div class="condidate_name">
              <p>Anita_C, 36 Female</p>
              <p class="address">Cumberland, New Jersey</p>
              <h6><a href="#">VIEW PROFILE</a></h6>
            </div>
          </div>
        </li>
        <li>
          <div class="images_pic"> <img src="{{ asset('imgs/default/Summersurf15.png') }}"> </div>
          <div class="age_value">
            <div class="condidate_name">
              <p>Summersurf15, 21 Female</p>
              <p class="address">Puebla, Mexico</p>
              <h6><a href="#">VIEW PROFILE</a></h6>
            </div>
          </div>
        </li>
      </ul>
      <div class="arrow">
        <img src="{{ asset('imgs/default/arrow_left.png') }}">
        <img src="{{ asset('imgs/default/righ_arow.png') }}">
      </div>
    </div>
  </div>
  <div class="footer">
    <div class="container">
      <div class="copiright col-lg-4 col-md-4 col-sm-4">
        <p>Danting my feet copyright</p>
      </div>
      <div class="naviget col-lg-4 col-md-4 col-sm-4">
        <ul class="list-inline">
          <li><a href="#">Terms of use</a></li>
          <li><a href="#">|</a></li>
          <li><a href="#">Privancy Policy</a></li>
          <li><a href="#">|</a></li>
          <li><a href="#">Contact us</a></li>
        </ul>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-4">
        <div class="social">
          <ul>
            <li>
              <a href="#"><img src="{{ asset('imgs/default/tw.jpg') }}"></a>
            </li>
            <li>
              <a href="#"><img src="{{ asset('imgs/default/fb.jpg') }}"></a>
            </li>
            <li>
              <a href="#"><img src="{{ asset('imgs/default/p.jpg') }}"></a>
            </li>
            <li>
              <a href="#"><img src="{{ asset('imgs/default/in.jpg') }}"></a>
            </li>
          </ul>
        </div>
      </div>
      </ul>
    </div>
  </div>
@stop
