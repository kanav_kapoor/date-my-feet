@extends('base')

@section('title')Register @stop

@section('css')
  <link href="{{ asset('css/default/style.css') }}" rel="stylesheet" type="text/css" />
@stop

@section('content')
  @include('_partials.header')
  @include('_partials.nav')
  <div class="banner">
    <div class="container">
      <div class="banner_content  col-lg-7 col-md-7 col-sm-7">
        <h1>Browse, connect and let your imagination run wild.</h1>
        <p>What are you waiting for? </p>
        <a href="#">Join Now</a> </div>
      <div class="right_articale col-lg-5 col-md-5 col-sm-5">
        {!! Form::open(['action' => 'Auth\AuthController@postRegister']) !!}
          <h2>SIGN UP</h2>
          <p>I am a:</p>
          <label>
            {!! Form::select('looking_for', $lookingFor) !!}
          </label>
          <p>Date of Birth:</p>
          <label>
            <div class="birth">
              {!! Form::select('date', $dates) !!}
              {!! Form::select('month', $months, null, ['class' => 'big_birth']) !!}
              {!! Form::select('year', $years) !!}
            </div>
          </label>
          <p>Location:</p>
          <label>
           {!! Form::select('country_id', $countries) !!}
          </label>
          <p>Email:</p>
          <label>
            {!! Form::text('email') !!}
            <span class="errors">{{ $errors->first('email') }}</span>
          </label>
          <p></p>
          <span class="activetion">Activation Link will be sent to this email!</span>
          <p>Password:</p>
          <label>
            {!! Form::password('password') !!}
            <span class="errors">{{ $errors->first('password') }}</span>            
          </label>
          {!! Form::submit('SIGN UP NOW') !!}
          <div class="form-control_text">
            <p>iam over 18. by clicking.on this button i confirm that i have read and agree to th<a href="#">Terms and conditoins Privacy </a>and<a href="#"> Cookie Policy</a></p>
          </div>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
  <div class="banner_text">
    <h1><i><a href="#">datingmyfeey.com</a> "where all of your sweet and feet dreams can come true"</i></h1>
  </div>
  <div class="gallery">
    <div class="container">
      <h2>most<a href="#"> online</a> members</h2>
      <ul>
        <li>
          <div class="images_pic"> <img src="{{asset('imgs/default/johannesM.png') }}"> </div>
          <div class="age_value">
            <div class="condidate_name">
              <p>JohannesM, 26 male </p>
              <p class="address"> Barcelona, Spain</p>
              <h6><a href="#">VIEW PROFILE</a></h6>
            </div>
          </div>
        </li>
        <li>
          <div class="images_pic"> <img src="{{asset('imgs/default/HotStuff418.png') }}"> </div>
          <div class="age_value">
            <div class="condidate_name">
              <p>HotStuff418, 22 Female</p>
              <p class="address">Trujillo, Puerto Rico</p>
              <h6><a href="#">VIEW PROFILE</a></h6>
            </div>
          </div>
        </li>
        <li>
          <div class="images_pic"> <img src="{{asset('imgs/default/SonjaG.png') }}"> </div>
          <div class="age_value">
            <div class="condidate_name">
              <p>SonjaG, 38 Female
              </p>
              <p class="address">West Bengal, India</p>
              <h6><a href="#">VIEW PROFILE</a></h6>
            </div>
          </div>
        </li>
        <li>
          <div class="images_pic"> <img src="{{asset('imgs/default/Cuti3g4l_99.png') }}"> </div>
          <div class="age_value">
            <div class="condidate_name">
              <p>Cuti3g4l_99, 18 Female
              </p>
              <p class="address">Williams, North Dakota</p>
              <h6><a href="#">VIEW PROFILE</a></h6>
            </div>
          </div>
        </li>
        <li>
          <div class="images_pic"> <img src="{{asset('imgs/default/Anita_C.png') }}"> </div>
          <div class="age_value">
            <div class="condidate_name">
              <p>Anita_C, 36 Female</p>
              <p class="address">Cumberland, New Jersey</p>
              <h6><a href="#">VIEW PROFILE</a></h6>
            </div>
          </div>
        </li>
        <li>
          <div class="images_pic"> <img src="{{asset('imgs/default/Summersurf15.png') }}"> </div>
          <div class="age_value">
            <div class="condidate_name">
              <p>Summersurf15, 21 Female</p>
              <p class="address">Puebla, Mexico</p>
              <h6><a href="#">VIEW PROFILE</a></h6>
            </div>
          </div>
        </li>
      </ul>
      <div class="arrow"> <img src="{{asset('imgs/default/arrow_left.png') }}"> <img src="{{asset('imgs/default/righ_arow.png') }}"> </div>
    </div>
  </div>
  <div class="about_us">
    <div class="container">
      <div class="content col-lg-6 col-md-6 col-sm-6">
        <h2>About dating<a href="#"> my feet</a></h2>
        <p>Welcome to Dating My Feet! Now you have the opportunity to meet real people who have a real fetish for the finer parts of the human body. On this site we bring sexy feet and toes together with those who love everything about those soft soles. </p>
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
      </div>
      <div class="heart col-lg-6 col-md-6 col-sm-6"> <img src="{{asset('imgs/default/heart.png') }}"> </div>
    </div>
  </div>
  <div class="workout">
    <div class="container">
      <h2>How it works</h2>
      <p>Get started on dating my feet today in 3 simple steps:</p>
      <ul>
        <li><img src="{{asset('imgs/default/creat_profil.jpg') }}">
          <h3>Creat A Profile</h3>
          <P>Create a personalised profile,
            <br> add photos and describe your
            <br> ideal partner</P>
        </li>
        <li><img src="{{asset('imgs/default/search.jpg') }}">
          <h3>Browse Photos</h3>
          <p>Find members based on
            <br> location, special interestes and
            <br> lifestyle preferences</p>
        </li>
        <li><img src="{{asset('imgs/default/last.jpg') }}">
          <h3>Strat Communicating</h3>
          <p>Show interest in the members
            <br> you like and let the journey
            <br> begin </p>
        </li>
      </ul>
      <a class="find" href="#">find your match</a> </div>
  </div>
  <div class="testimonial">
    <div class="container">
      <h2>testimonials</h2>
      <ul>
        <li>
          <p>A guy who loves to take care of my feet and who I can actually have a conversation with...what more can I ask for?</p>
          <a href="#">- Clarissa, Oakland, CA</a> </li>
        <li>
          <p>First date was a success. Of course I wore sandals just for him. ;)</p>
          <a href="#">- Sarai, Lake City, FL</a> </li>
        <li>
          <p>Who knew that so many women liked guys with foot fetishes?? I'm addicted to this site!</p>
          <a href="#">- Kevin, New York, NY</a> </li>
      </ul>
      <div class="arrow_testimonial"> <img class="left_arrow" src="{{asset('imgs/default/arrow_left.png') }}"> <img class="right_arrow" src="{{asset('imgs/default/righ_arow.png') }}"> </div>
    </div>
  </div>
  <div class="footer">
    <div class="container">
      <div class="copiright col-lg-4 col-md-4 col-sm-4">
        <p>Danting my feet copyright</p>
      </div>
      <div class="naviget col-lg-4 col-md-4 col-sm-4">
        <ul class="list-inline">
          <li><a href="#">Terms of use</a></li>
          <li><a href="#">|</a></li>
          <li><a href="#">Privancy Policy</a></li>
          <li><a href="#">|</a></li>
          <li><a href="#">Contact us</a></li>
        </ul>
      </div>
      <div class="col-lg-4 col-md-4 col-sm-4">
        <div class="social">
          <ul>
            <li>
              <a href="#"><img src="{{asset('imgs/default/tw.jpg') }}"></a>
            </li>
            <li>
              <a href="#"><img src="{{asset('imgs/default/fb.jpg') }}"></a>
            </li>
            <li>
              <a href="#"><img src="{{asset('imgs/default/p.jpg') }}"></a>
            </li>
            <li>
              <a href="#"><img src="{{asset('imgs/default/in.jpg') }}"></a>
            </li>
          </ul>
        </div>
      </div>
      </ul>
    </div>
  </div>
@stop
