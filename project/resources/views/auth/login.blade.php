@extends('base')

@section('title') Login - Dating my feet @stop
@section('css')
  <link rel="stylesheet" href="{{ asset('css/default/login.css') }}">
@stop
@section('content')
  <div class="top-content">
    <div class="inner-bg">
      <div class="row">
        <div class="col-sm-6 col-sm-offset-3 form-box">
          <div class="form-top">
            <div class="form-top-left">
              <h3>Login to our site</h3>
              <p>Enter your email and password to log on:</p>
            </div>
            <div class="form-top-right">
              <i class="fa fa-lock"></i>
            </div>
          </div>
          <div class="form-bottom">
            {!! Form::open(['action' => 'Auth\AuthController@postLogin', 'class' => 'login-form']) !!}
              <div class="form-group">
                <label class="sr-only" for="form-email">Email</label>
                {!! Form::text('email', null, ['placeholder' => 'Email...', 'class' => 'form-email form-control']) !!}
                 <span class="errors">{{ $errors->first('email') }}</span>
              </div>
              <div class="form-group">
                <label class="sr-only" for="form-password">Password</label>
                {!! Form::password('password', ['placeholder' => 'Password...', 'class' => 'form-email form-control']) !!}
                 <span class="errors">{{ $errors->first('password') }}</span>
              </div>
              <button type="submit" class="btn">Sign in!</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="clearfix"></div>
@stop