<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>@yield('title')</title>
	<link rel="stylesheet" href="{{ asset('libs/bootstrap/bootstrap.min.css') }}" type="text/css">
	<link rel="stylesheet" href="{{ asset('libs/bootstrap/bootstrap-theme.min.css') }}" type="text/css">
	<link href="{{ asset('libs/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
	@yield('css')
</head>
<body>
	@yield('content')
	<!-- JS -->
	<script src="{{ asset('libs/jquery/jquery.min.js') }}"></script>
  <script src="{{ asset('libs/bootstrap/bootstrap.min.js') }}"></script>
	@yield('js')
	<!--/ JS -->
</body>
</html>