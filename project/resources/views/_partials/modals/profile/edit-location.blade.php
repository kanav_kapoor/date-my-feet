<div class="modal fade" id="editLocation" role="dialog">
  <div class="modal-dialog modal-sm">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit Location</h4>
      </div>
      <div class="modal-body">
        {!! Form::text('location', null, ['class' => 'form-control', 'maxlength' => '30']) !!}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Update</button>
      </div>
    </div>
    <!--/ Modal content-->
  </div>
</div>