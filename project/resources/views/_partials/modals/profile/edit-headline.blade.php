<div class="modal fade" id="editHeadline" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit Headline</h4>
      </div>
      <div class="modal-body">
        {!! Form::textarea('headline', null, ['class' => 'form-control', 'rows' => '3', 'maxlength' => '140']) !!}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Update</button>
      </div>
    </div>
    <!--/ Modal content-->
  </div>
</div>