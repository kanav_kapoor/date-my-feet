<div class="modal fade" @yield('modal-id') role="dialog">
  <div class="modal-dialog @yield('modal-size')">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">@yield('modal-title')</h4>
      </div>
      <div class="modal-body">
        @yield('modal-body')
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
    <!--/ Modal content-->
  </div>
</div>