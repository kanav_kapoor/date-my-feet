<header>
  <div class="container">
    <div class="logo col-lg-5 col-md-5 col-sm-5">
      <a href="index.html"><img src="{{asset('imgs/default/logo.jpg') }}" /></a>
    </div>
    <div class="col-lg-4 col-md-4 col-md-offset-3 col-sm-4">
      <div class="login_btn">
        @if(Auth::check())
          <a href="{{action('Auth\AuthController@getLogout')}}">LOGOUT</a>
        @else
          <p>Already a member?</p>
          <a href="{{action('Auth\AuthController@getLogin')}}">LOGIN</a>
        @endif
      </div>
    </div>
  </div>
</header>