<?php

namespace Illuminate\Foundation\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Country;

trait RegistersUsers
{
    use RedirectsUsers;

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function getRegister()
    {
        $lookingFor = ['1' => 'Man Seeking a Woman', '2' => 'Women Seeking a Man'];
        $dates = range(01, 31);
        $months = [];
            for ($i=1; $i <= 12 ; $i++) $months[] = date("F", strtotime("2015-$i-01"));
        $years = range(1975, 1996);
        $countries = Country::lists('name', 'code');
        $data = compact('lookingFor', 'dates', 'months', 'years', 'countries');
        return view('auth.register', $data);
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postRegister(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            $this->throwValidationException(
                $request, $validator
            );
        }

        Auth::login($this->create($request->all()));

        return redirect($this->redirectPath());
    }
}
