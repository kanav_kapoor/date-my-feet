The current layout of the page looks good. I would just like to add a few things:
 
Tagline - this is where you have placed the phrase "I love sex". Keep it right where it is. Allow the user to only do a maximum of 140 characters when writing the tagline for their profile

The next thing is the description of themselves. Include: Gender, Age, Location, Sign, Height, Body Type (petite, average, athletic, bbw), sexual orientation, foot size (its important that this says foot size and not shoe size)
Limit the "About Me" and "About My Match" section to 140 characters. Add a section that says "Interesting Qualities" (this will include piercings, tattoos, very flexible, etc). Also add a section that says "My Foot Fetish Fantasy Perfect Date" (this is where they will describe their fantasy date) 

Everything else as you have it laid out on the page is great. We would like to only allow them to have up to 10 free pics associated with their monthly membership. They will have to pay for any additional photos. Can you do this with the payment gateway integration?